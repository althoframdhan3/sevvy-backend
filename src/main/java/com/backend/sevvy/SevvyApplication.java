package com.backend.sevvy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class SevvyApplication {

    public static void main(String[] args) {
        SpringApplication.run(SevvyApplication.class, args);
    }

}
