package com.backend.sevvy.pdfgenerator;

import com.backend.sevvy.database.model.Keterampilan;
import com.backend.sevvy.database.model.Situs;
import com.backend.sevvy.database.model.Users;
import java.awt.Color;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

public class TemplateFormal extends PDFTemplate {
    final static Color ACCENT_COLOR1 = Color.BLACK;
    final static Color ACCENT_COLOR2 = new Color(220, 53, 42);
    float marginLeftRight = 45;
    float marginTop = 50;

    public TemplateFormal(Users users, int colorCode, String titleLang) {
        super(users, colorCode, titleLang);
        setIndent(10);
    }

    @Override
    public void writeCV() throws IOException {
        writeName();
        writePersonalInfo();
        writeSitus();
        writePengalaman();
        writePendidikan();
        writePencapaian();
        writeKegiatanSukarela();
        writeKeterampilan();
    }

    @Override
    public void setupPage() throws IOException {
        setMaxTextLength(80);
        pageWidth = page.getMediaBox().getWidth();
        pageHeight = page.getMediaBox().getHeight();
        moveXPointer(marginLeftRight);

        setDocumentColor(colorCode);
        setDocumentFont();
    }

    @Override
    public void setDocumentFont() {
        headerFontSize = 28;
        bodyFontSize = 12;
        titleFontSize = 18;
        font = PDType1Font.TIMES_ROMAN;
        fontBold = PDType1Font.TIMES_BOLD;
    }

    @Override
    public void setDocumentColor(int colorCode) {
        bodyFontColor = Color.BLACK;

        switch (colorCode) {
            case 3:
                titleFontColor = ACCENT_COLOR1;
                headerFontColor = ACCENT_COLOR1;
                break;
            case 4:
                titleFontColor = ACCENT_COLOR2;
                headerFontColor = ACCENT_COLOR2;
                break;
        }
    }


    @Override
    public void writeName() throws IOException {
        cs.beginText();
        cs.setFont(fontBold, headerFontSize);
        cs.setNonStrokingColor(headerFontColor);
        cs.newLineAtOffset(marginLeftRight, page.getMediaBox().getHeight() - marginTop);
        moveYPointer(page.getMediaBox().getHeight() - marginTop);
        cs.showText(users.getPersonalInfo().getFirstName() + " ");
        cs.setFont(font, headerFontSize);
        cs.showText(users.getPersonalInfo().getLastName());
        cs.endText();
    }

    @Override
    public void writePersonalInfo() throws IOException {
        cs.beginText();
        cs.newLineAtOffset(xPointer, yPointer);
        cs.newLineAtOffset(0, -30);
        moveYPointer(-30);
        cs.setFont(font, bodyFontSize);
        cs.setNonStrokingColor(bodyFontColor);
        wrapText(new StringBuilder().append(users.getPersonalInfo().getEmail()).append(" | ")
                .append(users.getPersonalInfo().getPhoneNumber()).append(" | ")
                .append(users.getPersonalInfo().getAddress()).append(", ")
                .append(users.getPersonalInfo().getCity()).append(", ")
                .append(users.getPersonalInfo().getProvince()).append(", ")
                .append(users.getPersonalInfo().getPostalCode()).toString(), maxTextLength);
        cs.endText();

        //line
//        moveYPointer(-18);
        cs.setNonStrokingColor(bodyFontColor);
        cs.setLineWidth(1);
        cs.moveTo(xPointer, yPointer);
        cs.lineTo(pageWidth - marginLeftRight, yPointer);
        cs.stroke();
    }

    @Override
    public void writeSitus() throws IOException {
        if (!users.getSitusSet().isEmpty()) {
            cs.beginText();
            moveYPointer(-20);
            cs.newLineAtOffset(xPointer, yPointer);
            cs.setFont(font, bodyFontSize);
            cs.setNonStrokingColor(bodyFontColor);
            for (Situs situs : users.getSitusSet()) {
                cs.showText(situs.getLabel());
                cs.newLineAtOffset(60, 0);
                cs.showText(situs.getLink());
                cs.newLineAtOffset(-60, -lineHeight);
                moveYPointer(-lineHeight);
            }
            cs.endText();

            // line
            cs.setNonStrokingColor(bodyFontColor);
            cs.setLineWidth(1);
            cs.moveTo(marginLeftRight, yPointer);
            cs.lineTo(page.getMediaBox().getWidth() - marginLeftRight, yPointer);
            cs.stroke();
        }
    }

    @Override
    public void writeKeterampilan() throws IOException {
        if (!users.getKeterampilanSet().isEmpty()) {
            cs.beginText();
            cs.newLineAtOffset(xPointer, yPointer);

            cs.newLineAtOffset(0, -titleHeight);
            moveYPointer(-titleHeight);
            cs.setFont(fontBold, titleFontSize);
            cs.setNonStrokingColor(titleFontColor);
            cs.showText(titleLang.titleKeterampilan());

            cs.setFont(font, bodyFontSize);
            cs.setNonStrokingColor(bodyFontColor);
            cs.newLineAtOffset(indent, -subtitleHeight);
            moveYPointer(-subtitleHeight);
            for (Keterampilan keterampilan : users.getKeterampilanSet()) {
                cs.showText(keterampilan.getNamaKeterampilan());
                cs.newLineAtOffset(65, 0);
                cs.showText("(" + keterampilan.getTingkatKeterampilan() + "%)");
                cs.newLineAtOffset(-65, -lineHeight);
                moveYPointer(-lineHeight);
            }
            cs.endText();
        }
    }

}
