package com.backend.sevvy.pdfgenerator;

public interface TitleLanguage {

    public String titlePersonalInfo();

    public String titleSitus();

    public String titlePengalaman();

    public String titlePendidikan();

    public String titlePencapaian();

    public String titleKegiatanSukarela();

    public String titleKeterampilan();

}
