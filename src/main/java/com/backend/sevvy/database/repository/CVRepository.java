package com.backend.sevvy.database.repository;

import com.backend.sevvy.database.model.CV;
import com.backend.sevvy.database.model.Situs;
import com.backend.sevvy.database.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface CVRepository extends JpaRepository<CV,String> {
    List<CV> findAll();

    List<CV> findCVByUsers(Users users);

    CV findCVById(Long id);

    @Transactional
    void deleteById(Long id);
}

