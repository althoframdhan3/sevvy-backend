package com.backend.sevvy.database.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table
public class Pencapaian implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "institusi", nullable = false)
    private String institusi;

    @Column(name = "tahun")
    private int tahun;

    @Column(name = "deksripsi")
    private String deskripsi;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "email", updatable = false)
    private Users users;

    public Pencapaian() {

    }

    public Pencapaian(Long id, String title, String institusi, int tahun, String deskripsi) {
        this.id = id;
        this.title = title;
        this.institusi = institusi;
        this.tahun = tahun;
        this.deskripsi = deskripsi;
    }

    public String getTitle() {
        return title;
    }

    public String getInstitusi() {
        return institusi;
    }

    public int getTahun() {
        return tahun;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setInstitusi(String institusi) {
        this.institusi = institusi;
    }

    public void setTahun(int tahun) {
        this.tahun = tahun;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Long getId() {
        return id;
    }
}
