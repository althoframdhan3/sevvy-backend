package com.backend.sevvy.database.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table
public class Keterampilan implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "namaKeterampilan",nullable = false, updatable = false)
    private String namaKeterampilan;

    @Column(name = "tingkatKeterampilan", nullable = false)
    private int tingkatKeterampilan;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "email", updatable = false)

    private Users users;

    public Keterampilan(){

    }

    public Keterampilan(Long id, String namaKeterampilan, int tingkatKeterampilan) {
        this.id = id;
        this.namaKeterampilan = namaKeterampilan;
        this.tingkatKeterampilan = tingkatKeterampilan;
    }

    public int getTingkatKeterampilan() {
        return tingkatKeterampilan;
    }

    public String getNamaKeterampilan() {
        return namaKeterampilan;
    }

    public void setNamaKeterampilan(String namaKeterampilan) {
        this.namaKeterampilan = namaKeterampilan;
    }

    public void setTingkatKeterampilan(int tingkatKeterampilan) {
        this.tingkatKeterampilan = tingkatKeterampilan;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Long getId() {
        return id;
    }
}
