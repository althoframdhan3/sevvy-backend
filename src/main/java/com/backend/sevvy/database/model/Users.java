package com.backend.sevvy.database.model;

import java.io.Serializable;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class Users implements Serializable {
    @Id
    @Column(name = "email", updatable = false, nullable = false)
    private String email;

    @Column(name = "password", updatable = false, nullable = false)
    private String password;

    @OneToOne(mappedBy = "users", cascade = CascadeType.ALL)
    private PersonalInfo personalInfo;

    @OneToMany(mappedBy = "users", orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<KegiatanSukarela> kegiatanSukarelaSet = new HashSet<>();

    @OneToMany(mappedBy = "users", orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Keterampilan> keterampilanSet = new HashSet<>();

    @OneToMany(mappedBy = "users", orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Pencapaian> pencapaianSet = new HashSet<>();

    @OneToMany(mappedBy = "users", orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Pendidikan> pendidikanSet = new HashSet<>();

    @OneToMany(mappedBy = "users", orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Pengalaman> pengalamanSet = new HashSet<>();

    @OneToMany(mappedBy = "users", orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Situs> situsSet = new HashSet<>();

    @OneToMany(mappedBy = "users", orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<CV> CVSet = new HashSet<>();

    public Users() {

    }

    public Users(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public Set<Situs> getSitusSet() {
        return situsSet;
    }

    public Set<Pendidikan> getPendidikanSet() {
        return pendidikanSet;
    }

    public Set<Pengalaman> getPengalamanSet() {
        return pengalamanSet;
    }

    public Set<Pencapaian> getPencapaianSet() {
        return pencapaianSet;
    }

    public Set<KegiatanSukarela> getKegiatanSukarelaSet() {
        return kegiatanSukarelaSet;
    }

    public Set<Keterampilan> getKeterampilanSet() {
        return keterampilanSet;
    }

    public Set<CV> getCVSet() {
        return CVSet;
    }
}
