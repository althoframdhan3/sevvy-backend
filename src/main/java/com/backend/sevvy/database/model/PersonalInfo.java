package com.backend.sevvy.database.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "personalInfo")
public class PersonalInfo implements Serializable {
    @Column(name = "firstName", updatable = true, nullable = true)
    private String firstName;

    @Column(name = "lastName", updatable = true, nullable = true)
    private String lastName;

    @Id
    @Column(name = "email", updatable = false, nullable = false)
    private String email;

    @Column(name = "phoneNumber", updatable = true, nullable = true)
    private String phoneNumber;

    @Column(name = "address",updatable = true, nullable = true)
    private String address;

    @Column(name = "city", updatable = true, nullable = true)
    private String city;

    @Column(name = "province", updatable = true, nullable = true)
    private String province;

    @Column(name = "state", updatable = true, nullable = true)
    private String state;

    @Column(name = "postalCode", updatable = true, nullable = true)
    private String postalCode;

    @OneToOne
    @MapsId
    private Users users;

    public PersonalInfo(){

    }

    public PersonalInfo(String firstName, String lastName, String email,
                        String phoneNumber, String address, String city,
                        String province, String state, String postalCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.city = city;
        this.province = province;
        this.state = state;
        this.postalCode = postalCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getProvince() {
        return province;
    }

    public String getState() {
        return state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public void setUsers(Users users) {
        this.users = users;
    }
}
