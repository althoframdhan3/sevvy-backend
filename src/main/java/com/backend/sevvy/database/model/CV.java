package com.backend.sevvy.database.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table
public class CV implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "cvName")
    private String cvName;

    @Column(name = "template")
    private String template;

    @Column(name = "colorCode")
    private int colorCode;

    @Column(name = "cvLang")
    private String cvLang;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "email", updatable = false)
    private Users users;

    public CV(){

    }

    public CV(Long id, String cvName, String template, int colorCode, String cvLang) {
        this.id = id;
        this.cvName = cvName;
        this.template = template;
        this.colorCode = colorCode;
        this.cvLang = cvLang;
    }

    public Long getId() {
        return id;
    }

    public String getCvName() {
        return cvName;
    }

    public String getTemplate() {
        return template;
    }

    public String getCvLang() {
        return cvLang;
    }

    public void setCvName(String cvName) {
        this.cvName = cvName;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public void setColorCode(int colorCode) {
        this.colorCode = colorCode;
    }

    public void setCvLang(String cvLang) {
        this.cvLang = cvLang;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

}
