package com.backend.sevvy.database.service;

import com.backend.sevvy.database.model.CV;
import com.backend.sevvy.database.model.KegiatanSukarela;
import com.backend.sevvy.database.model.Keterampilan;
import com.backend.sevvy.database.model.Pencapaian;
import com.backend.sevvy.database.model.Pendidikan;
import com.backend.sevvy.database.model.Pengalaman;
import com.backend.sevvy.database.model.PersonalInfo;
import com.backend.sevvy.database.model.Situs;
import com.backend.sevvy.database.model.Users;
import com.backend.sevvy.database.repository.CVRepository;
import com.backend.sevvy.database.repository.KegiatanSukarelaRepository;
import com.backend.sevvy.database.repository.KeterampilanRepository;
import com.backend.sevvy.database.repository.PencapaianRepository;
import com.backend.sevvy.database.repository.PendidikanRepository;
import com.backend.sevvy.database.repository.PengalamanRepository;
import com.backend.sevvy.database.repository.PersonalInfoRepository;
import com.backend.sevvy.database.repository.SitusRepository;
import com.backend.sevvy.database.repository.UsersRepository;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class SevvyServiceImplTest {

    @Mock
    private UsersRepository usersRepository;
    @Mock
    private PersonalInfoRepository personalInfoRepository;
    @Mock
    private PencapaianRepository pencapaianRepository;
    @Mock
    private PendidikanRepository pendidikanRepository;
    @Mock
    private KeterampilanRepository keterampilanRepository;
    @Mock
    private KegiatanSukarelaRepository kegiatanSukarelaRepository;
    @Mock
    private PengalamanRepository pengalamanRepository;
    @Mock
    private SitusRepository situsRepository;
    @Mock
    private CVRepository cvRepository;
    @InjectMocks
    private SevvyServiceImpl sevvyService;

    @Mock
    Users users;
    Situs situs;
    Long id;
    PersonalInfo personalInfo;
    Pengalaman pengalaman;
    Pendidikan pendidikan;
    Pencapaian pencapaian;
    Keterampilan keterampilan;
    KegiatanSukarela kegiatanSukarela;
    CV cv;

    @BeforeEach
    public void setUp() {
        id = (long) 1;
//        users = new Users("dum@mail.com", "pw");
        situs = new Situs(id, "IG", "ig/nasywa.nf");
        personalInfo = new PersonalInfo("nasy", "f", "dum@mail.com", "0813",
                "kebonmanis", "bogor", "jabar", "jabar", "123");
        pengalaman = new Pengalaman(id, "ceo", "pacil", 2016, 2018, "abc");
        pendidikan = new Pendidikan(id, "SMA", "padma", "IPA", 2015, 2018);
        pencapaian = new Pencapaian(id, "j1", "pacil", 2015,"abc");
        keterampilan = new Keterampilan(id, "makan", 90);
        kegiatanSukarela = new KegiatanSukarela(id,"volunteer","pacil",2016,2018, "abc");
        cv = new CV(id, "satu", "plain", 1, "indo");
        sevvyService.addUser(users);
        kegiatanSukarela.setUsers(users);
        sevvyService.addKegiatanSukarela(users.getEmail(),kegiatanSukarela);


    }

    @Test
    public void testAddUsers() {
        Users users = new Users("dum@mail.com", "pw");
        sevvyService.addUser(users);

        verify(usersRepository, times(1)).save(users);
    }

    @Test
    public void testgetAll() {
        sevvyService.getAllUser();
        sevvyService.getAllKegiatanSukarela();
        sevvyService.getAllKeterampilan();
        sevvyService.getAllPencapaian();
        sevvyService.getAllPendidikan();
        sevvyService.getAllPengalaman();
        sevvyService.getAllPersonalInfo();
        sevvyService.getAllSitus();

        verify(usersRepository, times(1)).findAll();
        verify(kegiatanSukarelaRepository, times(1)).findAll();
        verify(keterampilanRepository, times(1)).findAll();
        verify(pencapaianRepository, times(1)).findAll();
        verify(pendidikanRepository, times(1)).findAll();
        verify(pengalamanRepository, times(1)).findAll();
        verify(personalInfoRepository, times(1)).findAll();
        verify(situsRepository, times(1)).findAll();
    }

    @Test
    public void testAddProfile() {
        users = new Users("dum@mail.com", "pw");
        kegiatanSukarela = new KegiatanSukarela((long)1, "a","a",2020,2020,"");
        keterampilan = new Keterampilan((long)1, "a",9);
        pencapaian = new Pencapaian();
        pengalaman = new Pengalaman();
        pendidikan = new Pendidikan();
        situs = new Situs();

        sevvyService.addUser(users);
        sevvyService.addKegiatanSukarela("dum@mail.com", kegiatanSukarela);
        sevvyService.addKeterampilan("dum@mail.com", keterampilan);
        sevvyService.addPencapaian("dum@mail.com", pencapaian);
        sevvyService.addPengalaman("dum@mail.com", pengalaman);
        sevvyService.addPendidikan("dum@mail.com",pendidikan);
        sevvyService.addSitus("dum@mail.com", situs);

        verify(kegiatanSukarelaRepository, times(1)).save(kegiatanSukarela);
        verify(situsRepository, times(1)).save(situs);
        verify(pengalamanRepository, times(1)).save(pengalaman);
        verify(keterampilanRepository, times(1)).save(keterampilan);
        verify(pencapaianRepository, times(1)).save(pencapaian);
        verify(pendidikanRepository, times(1)).save(pendidikan);
    }

    @Test
    public void testUpdateSitus() {
        users = new Users("dum@mail.com", "pw");
        users = usersRepository.save(users);
        situs = new Situs((long)1, "label", "link");
        sevvyService.addSitus("dum@mail.com", situs);
        situsRepository.save(situs);
        id =  situs.getId();

        situs = new Situs((long)1, "label2", "link");
        sevvyService.updateSitus(situs);
//        assertEquals("label2",sevvyService.findSitusById((long)1).getLabel());
        verify(situsRepository, times(1)).save(situs);
        verify(situsRepository, times(1)).save(situs);
    }

    @Test
    public void testUpdatePersonalInfo() {
        users = usersRepository.save(users);
        personalInfo = new PersonalInfo("", "","dum@mail.com","","","","","","");
        personalInfo.setUsers(users);
        personalInfoRepository.save(personalInfo);

        personalInfo = new PersonalInfo("a", "a","dum@mail.com","","","","","","");
        sevvyService.updatePersonalInfo(personalInfo);
        verify(personalInfoRepository, times(1)).save(personalInfo);
    }

    @Test
    public void testUpdateKegiatanSukarela() {
        Mockito.when(kegiatanSukarelaRepository.findKegiatanSukarelaById(id))
                .thenReturn(kegiatanSukarela);
        KegiatanSukarela kegiatanSukarelaAfter = new KegiatanSukarela(id,"ceo", "pacil",
                2016, 2018, "abc");
        KegiatanSukarela kegiatanSukarelaUpdated = sevvyService.findKegiatanSukarelaById(id);
        if (kegiatanSukarelaUpdated != null) {
            kegiatanSukarelaUpdated.setPerusahaan(kegiatanSukarelaAfter.getPerusahaan());
            kegiatanSukarelaUpdated.setPosisi(kegiatanSukarelaAfter.getPosisi());
            kegiatanSukarelaUpdated.setTahunMulai(kegiatanSukarelaAfter.getTahunMulai());
            kegiatanSukarelaUpdated.setTahunSelesai(kegiatanSukarelaAfter.getTahunSelesai());
            kegiatanSukarelaUpdated.setDeskripsi(kegiatanSukarelaAfter.getDeskripsi());
        }

        sevvyService.updateKegiatanSukarela(kegiatanSukarelaAfter);
        verify(kegiatanSukarelaRepository,times(1)).save(kegiatanSukarelaAfter);

    }

    @Test
    public void testDeleteProfile() {
        sevvyService.deleteKegiatanSukarela(id);
        sevvyService.deleteKeterampilan(id);
        sevvyService.deletePencapaian(id);
        sevvyService.deletePengalaman(id);
        sevvyService.deletePendidikan(id);
        sevvyService.deleteSitus(id);

        verify(kegiatanSukarelaRepository, times(1)).deleteById(id);
        verify(keterampilanRepository, times(1)).deleteById(id);
        verify(pencapaianRepository, times(1)).deleteById(id);
        verify(pengalamanRepository, times(1)).deleteById(id);
        verify(pendidikanRepository, times(1)).deleteById(id);
        verify(situsRepository, times(1)).deleteById(id);
    }

    @Test
    public void testFindProfileByEmail() {

        Users users = new Users("dum@mail.com", "pw");
        sevvyService.findKegiatanSukarelaByEmail("dum@mail.com");
        sevvyService.findKeterampilanByEmail("dum@mail.com");
        sevvyService.findPencapaianByEmail("dum@mail.com");
        sevvyService.findPendidikanByEmail("dum@mail.com");
        sevvyService.findPengalamanByEmail("dum@mail.com");
        sevvyService.findSitusByEmail("dum@mail.com");
        sevvyService.getPersonalInfo("dum@mail.com");

        Users user = sevvyService.findUserByEmail("dum@mail.com");
        verify(personalInfoRepository, times(1)).findByUsers(user);
        verify(kegiatanSukarelaRepository, times(1)).findKegiatanSukarelaByUsers(user);
        verify(keterampilanRepository, times(1)).findKeterampilanByUsers(user);
        verify(pencapaianRepository, times(1)).findPencapaianByUsers(user);
        verify(pendidikanRepository, times(1)).findPendidikanByUsers(user);
        verify(pengalamanRepository, times(1)).findPengalamanByUsers(user);
        verify(situsRepository, times(1)).findSitusByUsers(user);
    }


    @Test
    public void testFindProfileById() {
        sevvyService.findKegiatanSukarelaById((long)1);
        sevvyService.findKeterampilanById((long)2);
        sevvyService.findPencapaianById((long)3);
        sevvyService.findPendidikanById((long)4);
        sevvyService.findPengalamanById((long)5);
        sevvyService.findSitusById((long)6);

        verify(kegiatanSukarelaRepository, times(1)).findKegiatanSukarelaById((long)1);
        verify(keterampilanRepository, times(1)).findKeterampilanById((long)2);
        verify(pencapaianRepository, times(1)).findPencapaianById((long)3);
        verify(pendidikanRepository, times(1)).findPendidikanById((long)4);
        verify(pengalamanRepository, times(1)).findPengalamanById((long)5);
        verify(situsRepository, times(1)).findSitusById((long)6);
    }


    @Test
    public void testUpdateProfile() {
        sevvyService.updateKeterampilan(keterampilan);
        sevvyService.updatePencapaian(pencapaian);
        sevvyService.updatePendidikan(pendidikan);
        sevvyService.updatePengalaman(pengalaman);

        verify(keterampilanRepository, times(1)).save(keterampilan);
        verify(pencapaianRepository, times(1)).save(pencapaian);
        verify(pendidikanRepository, times(1)).save(pendidikan);
        verify(pengalamanRepository, times(1)).save(pengalaman);
    }

    @Test
    public void addCvTest() {
        cv = new CV(id, "satu", "plain", 1, "indo");
        Users newUser = new Users("a@a.com","1234");
        sevvyService.addCV("a@a.com", cv);
        verify(cvRepository, times(1)).save(cv);
    }

    @Test
    public void findCvByIdTest() {
        sevvyService.findCVById((long) 1);
        verify(cvRepository, times(1)).findCVById((long)1);
    }

    @Test
    public void findCvByEmailTest() {
        Mockito.when(sevvyService.findUserByEmail("dum@mail.com")).thenReturn(users);
        sevvyService.findCVByEmail("dum@mail.com");
        verify(cvRepository, times(1)).findCVByUsers(users);
    }

    @Test
    public void deleteCvTest() {
        sevvyService.deleteCV((long) 1);
        verify(cvRepository, times(1)).deleteById((long) 1);

    }

    @Test
    public void updateCvTest() {
        sevvyService.updateCV(cv);
        verify(cvRepository, times(1)).save(cv);
    }

    @Test
    public void downloadPdfTest() throws IOException {
        Set<Pengalaman> pengalamanSet = new HashSet<>();
        pengalamanSet.add(pengalaman);

        Set<Pendidikan> pendidikanSet = new HashSet<>();
        pendidikanSet.add(pendidikan);

        Set<Pencapaian> pencapaianSet = new HashSet<>();
        pencapaianSet.add(pencapaian);

        Set<Keterampilan> keterampilanSet = new HashSet<>();
        keterampilanSet.add(keterampilan);

        Set<KegiatanSukarela> kegiatanSukarelaSet = new HashSet<>();
        kegiatanSukarelaSet.add(kegiatanSukarela);

        Set<Situs> situsSet = new HashSet<>();
        situsSet.add(situs);

        Mockito.when(users.getPersonalInfo()).thenReturn(personalInfo);
        Mockito.when(users.getPengalamanSet()).thenReturn(pengalamanSet);
        Mockito.when(users.getKegiatanSukarelaSet()).thenReturn(kegiatanSukarelaSet);
        Mockito.when(users.getPendidikanSet()).thenReturn(pendidikanSet);
        Mockito.when(users.getPencapaianSet()).thenReturn(pencapaianSet);
        Mockito.when(users.getSitusSet()).thenReturn(situsSet);
        Mockito.when(users.getKeterampilanSet()).thenReturn(keterampilanSet);

        final byte[] downloadedFormalCC3Ind = sevvyService.downloadPDF(users, "FORMAL", 3, "IND");
        final byte[] downloadedFormalCC4Ind = sevvyService.downloadPDF(users, "FORMAL", 4, "IND");
        final byte[] downloadedFormalCC3Eng = sevvyService.downloadPDF(users, "FORMAL", 3, "ENG");
        final byte[] downloadedFormalCC4Eng = sevvyService.downloadPDF(users, "FORMAL", 4, "ENG");
        final byte[] downloadedModernlCC1Ind = sevvyService.downloadPDF(users, "MODERN", 1, "IND");
        final byte[] downloadedModernCC2Ind = sevvyService.downloadPDF(users, "MODERN", 2, "IND");
        final byte[] downloadedModernCC1Eng = sevvyService.downloadPDF(users, "MODERN", 1, "ENG");
        final byte[] downloadedModernCC2Eng = sevvyService.downloadPDF(users, "MODERN", 2, "ENG");
        assertEquals(true, downloadedFormalCC3Ind != null);
        assertEquals(true, downloadedFormalCC4Ind != null);
        assertEquals(true, downloadedFormalCC3Eng != null);
        assertEquals(true, downloadedFormalCC4Eng != null);

        assertEquals(true, downloadedModernlCC1Ind != null);
        assertEquals(true, downloadedModernCC2Ind != null);
        assertEquals(true, downloadedModernCC1Eng != null);
        assertEquals(true, downloadedModernCC2Eng != null);



    }



}
