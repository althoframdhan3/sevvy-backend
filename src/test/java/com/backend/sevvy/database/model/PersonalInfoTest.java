package com.backend.sevvy.database.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PersonalInfoTest {
    PersonalInfo personalInfo = new PersonalInfo();

    @BeforeEach
    public void setUp() {
        personalInfo = new PersonalInfo("nasywa","fathiyah","itsmenasywa@gmail.com",
                "012","kebonmanis","cilacap","jateng","Indonesia","524");
    }

    @Test
    public void testGetter() {
        assertEquals("nasywa",personalInfo.getFirstName());
        assertEquals("fathiyah",personalInfo.getLastName());
        assertEquals("itsmenasywa@gmail.com",personalInfo.getEmail());
        assertEquals("012",personalInfo.getPhoneNumber());
        assertEquals("kebonmanis",personalInfo.getAddress());
        assertEquals("cilacap",personalInfo.getCity());
        assertEquals("jateng",personalInfo.getProvince());
        assertEquals("Indonesia",personalInfo.getState());
        assertEquals("524",personalInfo.getPostalCode());
    }

    @Test
    public void testSetter() {
        personalInfo.setFirstName("nasywa1");
        personalInfo.setLastName("fathiyah1");
        personalInfo.setEmail("nasywa@gmail.com");
        personalInfo.setPhoneNumber("0123");
        personalInfo.setAddress("kebon");
        personalInfo.setCity("cilacapop");
        personalInfo.setProvince("jatengg");
        personalInfo.setState("indo");
        personalInfo.setPostalCode("5245");
        assertEquals("nasywa1",personalInfo.getFirstName());
        assertEquals("fathiyah1",personalInfo.getLastName());
        assertEquals("nasywa@gmail.com",personalInfo.getEmail());
        assertEquals("0123",personalInfo.getPhoneNumber());
        assertEquals("kebon",personalInfo.getAddress());
        assertEquals("cilacapop",personalInfo.getCity());
        assertEquals("jatengg",personalInfo.getProvince());
        assertEquals("indo",personalInfo.getState());
        assertEquals("5245",personalInfo.getPostalCode());
    }

}
