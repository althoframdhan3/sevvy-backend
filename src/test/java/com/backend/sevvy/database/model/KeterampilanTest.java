package com.backend.sevvy.database.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class KeterampilanTest {
    Keterampilan keterampilan = new Keterampilan();

    @BeforeEach
    public void setUp() {
        keterampilan = new Keterampilan((long)1, "mencuci",90);

    }

    @Test
    public void testNamaGetter() {
        assertEquals("mencuci",keterampilan.getNamaKeterampilan());
    }

    @Test
    public void testTingkatGetter() {
        assertEquals(90,keterampilan.getTingkatKeterampilan());
    }

    @Test
    public void testIdGetter() {
        assertEquals((long)1, keterampilan.getId());
    }

    @Test
    public void testNamaSetter() {
        keterampilan.setNamaKeterampilan("abc");
        assertEquals("abc",keterampilan.getNamaKeterampilan());
    }

    @Test
    public void testTingkatSetter() {
        keterampilan.setTingkatKeterampilan(80);
        assertEquals(80,keterampilan.getTingkatKeterampilan());
    }
}
