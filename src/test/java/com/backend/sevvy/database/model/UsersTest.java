package com.backend.sevvy.database.model;

import com.backend.sevvy.database.repository.UsersRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UsersTest {
    @Mock
    private UsersRepository usersRepository;
    Users users = new Users();

    @BeforeEach
    public void setUp() {
        users = new Users("dumdum@mail.com","pw");
    }

    @Test
    public void testGetter() {
        assertEquals("dumdum@mail.com",users.getEmail());
        assertEquals("pw",users.getPassword());
    }

    @Test
    public void testGetterSet() {
        usersRepository.save(users);
        assertEquals(true, users.getKegiatanSukarelaSet().isEmpty());
        assertEquals(true, users.getKeterampilanSet().isEmpty());
        assertEquals(true, users.getPencapaianSet().isEmpty());
        assertEquals(true, users.getPendidikanSet().isEmpty());
        assertEquals(true, users.getSitusSet().isEmpty());
        assertEquals(true, users.getPengalamanSet().isEmpty());
        assertEquals(true, users.getCVSet().isEmpty());
    }
}
