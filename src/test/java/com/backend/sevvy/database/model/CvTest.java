package com.backend.sevvy.database.model;

import com.backend.sevvy.database.repository.CVRepository;
import com.backend.sevvy.database.repository.UsersRepository;
import com.backend.sevvy.database.service.SevvyServiceImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;


public class CvTest {
    CV cv;
    Users users;

    @Mock
    private CVRepository cvRepository;
    private UsersRepository usersRepository;

    @InjectMocks
    private SevvyServiceImpl sevvyService;

    @BeforeEach
    public void setUp() {
        cv = new CV((long) 1, "satu", "plain", 1, "malay");
        users = new Users("a@a.com", "abc");
    }

    @Test
    public void getterTest() {
        assertEquals(1, cv.getId());
        assertEquals("satu", cv.getCvName());
        assertEquals("plain", cv.getTemplate());
        assertEquals("malay", cv.getCvLang());
    }

    @Test
    public void setterTest() {
        cv.setCvName("a");
        cv.setColorCode(1);
        cv.setCvLang("indo");
        cv.setUsers(users);
        cv.setTemplate("ab");
        assertEquals("a", cv.getCvName());
        assertEquals("ab", cv.getTemplate());
        assertEquals("indo", cv.getCvLang());
    }


}
