package com.backend.sevvy.database.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



public class PencapaianTest {
    Pencapaian pencapaian = new Pencapaian();

    @BeforeEach
    public void setUp() {
        pencapaian = new Pencapaian((long)1, "juara 1","BEM UI",2020, "test");
    }

    @Test
    public void testTitleGetter() {
        assertEquals("juara 1",pencapaian.getTitle());
    }

    @Test
    public void testInstitusiGetter() {
        assertEquals("BEM UI",pencapaian.getInstitusi());
    }

    @Test
    public void testTahunGetter() {
        assertEquals(2020,pencapaian.getTahun());
    }

    @Test
    public void testDeskripsiGetter() {
        assertEquals("test",pencapaian.getDeskripsi());
    }

    @Test
    public void testIdGetter() {
        assertEquals((long)1, pencapaian.getId());
    }

    @Test
    public void testTitleSetter() {
        pencapaian.setTitle("a");
        assertEquals("a",pencapaian.getTitle());
    }

    @Test
    public void testInstitusiSetter() {
        pencapaian.setInstitusi("b");
        assertEquals("b",pencapaian.getInstitusi());
    }

    @Test
    public void testDeskripsiSetter() {
        pencapaian.setDeskripsi("c");
        assertEquals("c",pencapaian.getDeskripsi());
    }

    @Test
    public void testTahunSetter() {
        pencapaian.setTahun(2020);
        assertEquals(2020,pencapaian.getTahun());
    }
}
