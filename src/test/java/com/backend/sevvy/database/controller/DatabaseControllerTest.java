package com.backend.sevvy.database.controller;

import com.backend.sevvy.database.model.CV;
import com.backend.sevvy.database.model.KegiatanSukarela;
import com.backend.sevvy.database.model.Keterampilan;
import com.backend.sevvy.database.model.Pencapaian;
import com.backend.sevvy.database.model.Pendidikan;
import com.backend.sevvy.database.model.Pengalaman;
import com.backend.sevvy.database.model.PersonalInfo;
import com.backend.sevvy.database.model.Situs;
import com.backend.sevvy.database.model.Users;
import com.backend.sevvy.database.service.SevvyService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DatabaseControllerTest {
    Users user;
    PersonalInfo person;
    @Autowired
    private MockMvc mvc;
    @MockBean
    private SevvyService mockSevvyService;

    @Before
    public void setUp() throws IOException {
        Users user = new Users("barbie.girl@gmail.com", "abc");
        PersonalInfo person = new PersonalInfo("Barbie0", "Girl", "barbie0.girl@gmail.com",
                "0852881088888", "Barbie world", "Magic", "Plastic", "Funtastic", "16421");
        mockSevvyService.addUser(user);
    }

    //USER
    @Test
    public void createUser() throws Exception {
        String inputJson = new ObjectMapper().writeValueAsString(new Users("barbie.girl@gmail.com",
                "abc"));
        this.mvc.perform(post("/database/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson))
                .andExpect(status().isOk());
    }


    @Test
    public void getAllUsersTest() throws Exception {
        PersonalInfo personalInfo = new PersonalInfo();

        Users user1 = new Users("a@a.com", "1234");
        PersonalInfo personalInfo1 = new PersonalInfo("nasywa", "fathiyah", "a@a.com",
                "012", "kebonmanis", "cilacap", "jateng", "Indonesia", "524");

        List<Users> usersList = new ArrayList<>();

        usersList.add(user1);


        Mockito.when(mockSevvyService.getAllUser())
                .thenReturn(usersList);

        String uri = "/database/users/all";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mvc.perform(requestBuilder).andReturn();

        String expected = "[{\"email\":\"a@a.com\","
                + "\"password\":\"1234\","
                + "\"personalInfo\":null,"
                + "\"kegiatanSukarelaSet\":[],"
                + "\"keterampilanSet\":[],"
                + "\"pencapaianSet\":[],"
                + "\"pendidikanSet\":[],"
                + "\"pengalamanSet\":[],"
                + "\"situsSet\":[],"
                + "\"cvset\":[]}]";

        String expectedJson = new Gson().toJson(usersList);

        String outputInJson = result.getResponse().getContentAsString();

        assertThat(outputInJson).isEqualTo(expected);

    }

    @Test
    public void getUserTest() throws Exception {
        String inputJson = new ObjectMapper().writeValueAsString(new Users("a@a.com", "1234"));
        Users users = new Users("a@a.com", "1234");
        List<Users> usersList = new ArrayList<>();

        usersList.add(users);

        Mockito.when(mockSevvyService.findUserByEmail("a@a.com"))
                .thenReturn(users);

        String uri = "/database/users/?email=a@a.com";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mvc.perform(requestBuilder).andReturn();
        String outputInJson = result.getResponse().getContentAsString();
        String expected = "{\"email\":\"a@a.com\","
                + "\"password\":\"1234\","
                + "\"personalInfo\":null,"
                + "\"kegiatanSukarelaSet\":[],"
                + "\"keterampilanSet\":[],"
                + "\"pencapaianSet\":[],"
                + "\"pendidikanSet\":[],"
                + "\"pengalamanSet\":[],"
                + "\"situsSet\":[],"
                + "\"cvset\":[]}";

        assertThat(outputInJson).isEqualTo(expected);


    }

    //PersonalInfo
    @Test
    public void updatePersonalInfoTest() throws Exception {
        PersonalInfo personalInfo = new PersonalInfo("nasywa", "fathiyah", "barbie.girl@gmail.com",
                "012", "kebonmanis", "cilacap", "jateng", "Indonesia", "524");
        String inputJson = new ObjectMapper().writeValueAsString(personalInfo);
        String uri = "/database/profile/personal";

        this.mvc.perform(put(uri)
                .contentType(MediaType.APPLICATION_JSON).content(inputJson))
                .andExpect(status().isOk());
    }

    @Test
    public void getPersonalInfoTest() throws Exception {
        PersonalInfo personalInfo1 = new PersonalInfo("nasywa", "fathiyah", "barbie.girl@gmail.com",
                "012", "kebonmanis", "cilacap", "jateng", "Indonesia", "524");
        PersonalInfo personalInfo2 = new PersonalInfo("nnf", "fathiyah", "barbie.girl@gmail.com",
                "012", "kebonmanis", "cilacap", "jateng", "Indonesia", "524");

        List<PersonalInfo> personalInfoList = new ArrayList<>();
        personalInfoList.add(personalInfo1);
        personalInfoList.add(personalInfo2);

        String inputJson = new ObjectMapper().writeValueAsString(personalInfoList);
        String uri = "/database/profile/personal/?email=barbie.girl@gmail.com";

        Mockito.when(
                mockSevvyService.getPersonalInfo("barbie.girl@gmail.com"))
                .thenReturn(personalInfoList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        MvcResult result = mvc.perform(requestBuilder).andReturn();
        String expected = new Gson().toJson(personalInfoList);
        String output = result.getResponse().getContentAsString();

        assertThat(output).isEqualTo(expected);

    }

    //SITUS
    @Test
    public void addSitusTest() throws Exception {
        String inputJson = new ObjectMapper().writeValueAsString(new Situs((long) 1, "Instagram",
                "instagram.com/barbie"));
        this.mvc.perform(post("/database/profile/situs/barbie.girl@gmail.com")
                .contentType(MediaType.APPLICATION_JSON).content(inputJson))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllSitusTest() throws Exception {
        Situs situs1 = new Situs((long) 1, "IG", "ig/nasywa.nf");
        Situs situs2 = new Situs((long) 2, "twittah", "nasywanf_");
        situs1.setUsers(user);
        situs2.setUsers(user);
        List<Situs> situsList = new ArrayList<>();
        situsList.add(situs1);
        situsList.add(situs2);

        String inputJson = new ObjectMapper().writeValueAsString(situsList);
        String uri = "/database/profile/situs/all/?email=barbie.girl@gmail.com";

        Mockito.when(
                mockSevvyService.findSitusByEmail("barbie.girl@gmail.com"))
                .thenReturn(situsList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        MvcResult result = mvc.perform(requestBuilder).andReturn();
        String expected = new Gson().toJson(situsList);
        String output = result.getResponse().getContentAsString();

        assertThat(output).isEqualTo(expected);
    }

    @Test
    public void getSitusTest() throws Exception {
        Situs situs1 = new Situs((long) 1, "IG", "ig/nasywa.nf");
        Situs situs2 = new Situs((long) 2, "twittah", "nasywanf_");
        List<Situs> situsList = new ArrayList<>();
        situsList.add(situs1);
        situsList.add(situs2);

        String inputJson = new ObjectMapper().writeValueAsString(situsList);
        String uri = "/database/profile/situs/?id=1";

        Mockito.when(
                mockSevvyService.findSitusById((long) 1))
                .thenReturn(situs1);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        MvcResult result = mvc.perform(requestBuilder).andReturn();
        String expected = new Gson().toJson(situs1);
        String output = result.getResponse().getContentAsString();

        assertThat(output).isEqualTo(expected);

    }

    @Test
    public void updateSitusTest() throws Exception {
        Situs situs = new Situs((long) 1, "IG", "ig/nasywa.nf");
        String situsJson = new ObjectMapper().writeValueAsString(situs);
        Mockito.when(
                mockSevvyService.updateSitus(situs))
                .thenReturn(situs);
        String uri = "/database/profile/situs";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .put(uri)
                .accept(MediaType.APPLICATION_JSON)
                .content(situsJson)
                .contentType(MediaType.APPLICATION_JSON);

        this.mvc.perform(put("/database/profile/situs/")
                .contentType(MediaType.APPLICATION_JSON).content(situsJson))
                .andExpect(status().isOk());

    }

    @Test
    public void deleteSitus() throws Exception {
        String input = "{\"id\":\"1\","
                + "\"label\":\"IG\","
                + "\"link\":\"ig/nasywa.nf\"}";

        String uriInput = "/database/profile/situs/barbie.girl@gmail.com";
        String uri = "/database/profile/situs/?id=1";
        RequestBuilder requestBuilderAdd = MockMvcRequestBuilders
                .post(uriInput)
                .accept(MediaType.APPLICATION_JSON)
                .content(input)
                .contentType(MediaType.APPLICATION_JSON);

        RequestBuilder requestBuilderDelete = MockMvcRequestBuilders
                .delete(uri)
                .accept(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilderAdd).andExpect(status().isOk());
        mvc.perform(requestBuilderDelete).andExpect(status().isOk());

    }

    //Pendidikan
    @Test
    public void addPendidikanTest() throws Exception {
        Pendidikan pendidikan = new Pendidikan((long) 1, "SMA", "Barbie High School",
                "IPA", 2012, 2016);
        pendidikan.setUsers(user);
        String inputJson = new ObjectMapper().writeValueAsString(pendidikan);
        this.mvc.perform(post("/database/profile/pendidikan/barbie.girl@gmail.com")
                .contentType(MediaType.APPLICATION_JSON).content(inputJson))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllPendidikanTest() throws Exception {
        Pendidikan pendidikan1 = new Pendidikan((long) 1, "SMA", "Padma", "IPA", 2015, 2018);
        Pendidikan pendidikan2 = new Pendidikan((long) 2, "SMA", "Tld", "Sos", 2015, 2018);
        pendidikan1.setUsers(user);
        pendidikan2.setUsers(user);
        List<Pendidikan> pendidikanList = new ArrayList<>();
        pendidikanList.add(pendidikan1);
        pendidikanList.add(pendidikan2);

        String inputJson = new ObjectMapper().writeValueAsString(pendidikanList);
        String uri = "/database/profile/pendidikan/all/?email=barbie.girl@gmail.com";

        Mockito.when(
                mockSevvyService.findPendidikanByEmail("barbie.girl@gmail.com"))
                .thenReturn(pendidikanList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        MvcResult result = mvc.perform(requestBuilder).andReturn();
        String expected = new Gson().toJson(pendidikanList);
        String output = result.getResponse().getContentAsString();

        assertThat(output).isEqualTo(expected);

    }

    @Test
    public void getPendidikanTest() throws Exception {
        Pendidikan pendidikan1 = new Pendidikan((long) 1, "SMA", "Padma", "IPA", 2015, 2018);
        Pendidikan pendidikan2 = new Pendidikan((long) 2, "SMA", "Tld", "Sos", 2015, 2018);
        pendidikan1.setUsers(user);
        pendidikan2.setUsers(user);
        List<Pendidikan> pendidikanList = new ArrayList<>();
        pendidikanList.add(pendidikan1);
        pendidikanList.add(pendidikan2);
        String inputJson = new ObjectMapper().writeValueAsString(pendidikan1);
        String uri = "/database/profile/pendidikan/?id=1";

        Mockito.when(
                mockSevvyService.findPendidikanById((long) 1))
                .thenReturn(pendidikan1);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        MvcResult result = mvc.perform(requestBuilder).andReturn();
        String expected = new Gson().toJson(pendidikan1);
        String output = result.getResponse().getContentAsString();

        assertThat(output).isEqualTo(expected);

    }

    @Test
    public void findPendidikanTest() throws Exception {
        Pendidikan pendidikan = new Pendidikan((long) 1, "SMA", "Padma", "IPA", 2015, 2018);
        String pendidikanJson = new ObjectMapper().writeValueAsString(pendidikan);
        Mockito.when(
                mockSevvyService.updatePendidikan(pendidikan))
                .thenReturn(pendidikan);
        String uri = "/database/profile/pendidikan";

        this.mvc.perform(put("/database/profile/pendidikan/")
                .contentType(MediaType.APPLICATION_JSON).content(pendidikanJson))
                .andExpect(status().isOk());
    }

    @Test
    public void deletePendidikanTest() throws Exception {
        String input = "{\"id\":\"1\","
                + "\"jenjang\":\"SMA\","
                + "\"namaInstitusi\":\"padma\","
                + "\"jurusan\":\"IPA\","
                + "\"tahunMulai\":\"2015\","
                + "\"tahunSelesai\":\"2018\"}";

        String uriInput = "/database/profile/pendidikan/barbie.girl@gmail.com";
        String uri = "/database/profile/pendidikan/?id=1";
        RequestBuilder requestBuilderAdd = MockMvcRequestBuilders
                .post(uriInput)
                .accept(MediaType.APPLICATION_JSON)
                .content(input)
                .contentType(MediaType.APPLICATION_JSON);

        RequestBuilder requestBuilderDelete = MockMvcRequestBuilders
                .delete(uri)
                .accept(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilderAdd).andExpect(status().isOk());
        mvc.perform(requestBuilderDelete).andExpect(status().isOk());
    }

    //Pengalaman
    @Test
    public void createPengalaman() throws Exception {
        String inputJson = new ObjectMapper().writeValueAsString(new Pengalaman((long) 1,
                "Asdos Adprog", "Fasilkom", 2019, 2020, "Mengajar adprog"));
        this.mvc.perform(post("/database/profile/pengalaman/barbie.girl@gmail.com")
                .contentType(MediaType.APPLICATION_JSON).content(inputJson))
                .andExpect(status().isOk());
    }

    @Test
    public void getPengalamanTest() throws Exception {
        Pengalaman pengalaman1 = new Pengalaman((long) 1, "ceo", "pacil", 2016, 2018, "abc");
        Pengalaman pengalaman2 = new Pengalaman((long) 2, "cto", "pacil", 2016, 2018, "abc");
        pengalaman1.setUsers(user);
        pengalaman2.setUsers(user);
        List<Pengalaman> pengalamanList = new ArrayList<>();
        pengalamanList.add(pengalaman1);
        pengalamanList.add(pengalaman2);

        String inputJson = new ObjectMapper().writeValueAsString(pengalaman1);
        String uri = "/database/profile/pengalaman/?id=1";

        Mockito.when(
                mockSevvyService.findPengalamanById((long) 1))
                .thenReturn(pengalaman1);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        MvcResult result = mvc.perform(requestBuilder).andReturn();
        String expected = new Gson().toJson(pengalaman1);
        String output = result.getResponse().getContentAsString();

        assertThat(output).isEqualTo(expected);

    }

    @Test
    public void getAllPengalamanTest() throws Exception {
        Pengalaman pengalaman1 = new Pengalaman((long) 1, "ceo", "pacil",
                2016, 2018, "abc");
        Pengalaman pengalaman2 = new Pengalaman((long) 2, "cto", "pacil",
                2016, 2018, "abc");
        pengalaman1.setUsers(user);
        pengalaman2.setUsers(user);
        List<Pengalaman> pengalamanList = new ArrayList<>();
        pengalamanList.add(pengalaman1);
        pengalamanList.add(pengalaman2);

        String inputJson = new ObjectMapper().writeValueAsString(pengalaman1);
        String uri = "/database/profile/pengalaman/all/?email=barbie.girl@gmail.com";

        Mockito.when(
                mockSevvyService.findPengalamanByEmail("barbie.girl@gmail.com"))
                .thenReturn(pengalamanList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        MvcResult result = mvc.perform(requestBuilder).andReturn();
        String expected = new Gson().toJson(pengalamanList);
        String output = result.getResponse().getContentAsString();

        assertThat(output).isEqualTo(expected);
    }


    @Test
    public void findPengalamanTest() throws Exception {
        Pengalaman pengalamanAfter = new Pengalaman((long) 1, "cto", "pacil",
                2016, 2018, "abc");
        String pengalamanAfterJson = new ObjectMapper().writeValueAsString(pengalamanAfter);
        Mockito.when(
                mockSevvyService.updatePengalaman(pengalamanAfter))
                .thenReturn(pengalamanAfter);
        String uri = "/database/profile/pengalaman";

        this.mvc.perform(put("/database/profile/pengalaman/")
                .contentType(MediaType.APPLICATION_JSON).content(pengalamanAfterJson))
                .andExpect(status().isOk());
    }

    @Test
    public void deletePengalamananTest() throws Exception {
        String input = "{\"id\":\"1\","
                + "\"posisi\":\"ceo\","
                + "\"perusahaan\":\"pacil\","
                + "\"periodeMulai\":\"2016\","
                + "\"periodeSelesai\":\"2018\","
                + "\"deskripsi\":\"abc\"}";

        String uriInput = "/database/profile/pengalaman/barbie.girl@gmail.com";
        String uri = "/database/profile/pengalaman/?id=1";
        RequestBuilder requestBuilderAdd = MockMvcRequestBuilders
                .post(uriInput)
                .accept(MediaType.APPLICATION_JSON)
                .content(input)
                .contentType(MediaType.APPLICATION_JSON);

        RequestBuilder requestBuilderDelete = MockMvcRequestBuilders
                .delete(uri)
                .accept(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilderAdd).andExpect(status().isOk());
        mvc.perform(requestBuilderDelete).andExpect(status().isOk());
    }

    //Pencapaian
    @Test
    public void createPencapaianTest() throws Exception {
        String inputJson = new ObjectMapper().writeValueAsString(new Pencapaian((long) 1,
                "juara 1 menyanyi", "barbie land", 2012, "se-barbie world"));
        this.mvc.perform(post("/database/profile/pencapaian/barbie.girl@gmail.com")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllPencapaianTest() throws Exception {
        Pencapaian pencapaian1 = new Pencapaian((long) 1, "juara 1", "pacil", 2016, "abc");
        Pencapaian pencapaian2 = new Pencapaian((long) 2, "juara 2", "a", 2016, "abc");
        pencapaian1.setUsers(user);
        pencapaian2.setUsers(user);
        List<Pencapaian> pencapaianList = new ArrayList<>();
        pencapaianList.add(pencapaian1);
        pencapaianList.add(pencapaian2);

        String inputJson = new ObjectMapper().writeValueAsString(pencapaian1);
        String uri = "/database/profile/pencapaian/all/?email=barbie.girl@gmail.com";

        Mockito.when(
                mockSevvyService.findPencapaianByEmail("barbie.girl@gmail.com"))
                .thenReturn(pencapaianList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        MvcResult result = mvc.perform(requestBuilder).andReturn();
        String expected = new Gson().toJson(pencapaianList);
        String output = result.getResponse().getContentAsString();

        assertThat(output).isEqualTo(expected);

    }

    @Test
    public void getPencapaianTest() throws Exception {
        Pencapaian pencapaian1 = new Pencapaian((long) 1, "juara 1", "pacil", 2016, "abc");
        pencapaian1.setUsers(user);

        String inputJson = new ObjectMapper().writeValueAsString(pencapaian1);
        String uri = "/database/profile/pencapaian/?id=1";

        Mockito.when(
                mockSevvyService.findPencapaianById((long) 1))
                .thenReturn(pencapaian1);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        MvcResult result = mvc.perform(requestBuilder).andReturn();
        String expected = new Gson().toJson(pencapaian1);
        String output = result.getResponse().getContentAsString();

        assertThat(output).isEqualTo(expected);
    }

    @Test
    public void updatePencapaianTest() throws Exception {
        Pencapaian pencapaianBefore = new Pencapaian((long) 1, "juara 2", "apa", 2016, "cba");
        mockSevvyService.addPencapaian("barbie.girl@gmail.com",pencapaianBefore);
        Pencapaian pencapaian = new Pencapaian((long) 1, "juara 1", "pacil", 2016, "abc");

        String pencapianJson = new ObjectMapper().writeValueAsString(pencapaian);
        Mockito.when(
                mockSevvyService.updatePencapaian(pencapaian))
                .thenReturn(pencapaian);
        this.mvc.perform(put("/database/profile/pencapaian/")
                .contentType(MediaType.APPLICATION_JSON).content(pencapianJson))
                .andExpect(status().isOk());
    }

    @Test
    public void deletePencapaianTest() throws Exception {
        String input = "{\"id\":\"1\","
                + "\"title\":\"juara 1\","
                + "\"institusi\":\"pacil\","
                + "\"tahun\":\"2016\","
                + "\"deskripsi\":\"abc\"}";

        String uriInput = "/database/profile/pencapaian/barbie.girl@gmail.com";
        String uri = "/database/profile/pencapaian/?id=1";
        RequestBuilder requestBuilderAdd = MockMvcRequestBuilders
                .post(uriInput)
                .accept(MediaType.APPLICATION_JSON)
                .content(input)
                .contentType(MediaType.APPLICATION_JSON);

        RequestBuilder requestBuilderDelete = MockMvcRequestBuilders
                .delete(uri)
                .accept(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilderAdd).andExpect(status().isOk());
        mvc.perform(requestBuilderDelete).andExpect(status().isOk());
    }

    //KegiatanSukarela
    @Test
    public void createKegiatanSukarelaTest() throws Exception {
        String inputJson = new ObjectMapper().writeValueAsString(new KegiatanSukarela((long) 1,
                "pacil", "ceo", 2016, 2018, "abc"));
        this.mvc.perform(post("/database/profile/kegiatan-sukarela/barbie.girl@gmail.com")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllKegiatanSukarelaTest() throws Exception {
        KegiatanSukarela kegiatanSukarela1 = new KegiatanSukarela((long) 1, "pacil", "ceo",
                2016, 2018, "abc");
        KegiatanSukarela kegiatanSukarela2 = new KegiatanSukarela((long) 2, "pusil", "sekre",
                2017, 2019, "avb");
        kegiatanSukarela1.setUsers(user);
        kegiatanSukarela2.setUsers(user);
        List<KegiatanSukarela> kegiatanSukarelaList = new ArrayList<>();
        kegiatanSukarelaList.add(kegiatanSukarela1);
        kegiatanSukarelaList.add(kegiatanSukarela2);

        String inputJson = new ObjectMapper().writeValueAsString(kegiatanSukarelaList);
        String uri = "/database/profile/kegiatan-sukarela/all/?email=barbie.girl@gmail.com";

        Mockito.when(
                mockSevvyService.findKegiatanSukarelaByEmail("barbie.girl@gmail.com"))
                .thenReturn(kegiatanSukarelaList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        MvcResult result = mvc.perform(requestBuilder).andReturn();
        String expected = new Gson().toJson(kegiatanSukarelaList);
        String output = result.getResponse().getContentAsString();

        assertThat(output).isEqualTo(expected);

    }

    @Test
    public void getKegiatanSukarelaTest() throws Exception {
        KegiatanSukarela kegiatanSukarela1 = new KegiatanSukarela((long) 1,
                "pacil", "ceo", 2016, 2018, "abc");
        kegiatanSukarela1.setUsers(user);

        String inputJson = new ObjectMapper().writeValueAsString(kegiatanSukarela1);
        String uri = "/database/profile/kegiatan-sukarela/?id=1";

        Mockito.when(
                mockSevvyService.findKegiatanSukarelaById((long) 1))
                .thenReturn(kegiatanSukarela1);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        MvcResult result = mvc.perform(requestBuilder).andReturn();
        String expected = new Gson().toJson(kegiatanSukarela1);
        String output = result.getResponse().getContentAsString();

        assertThat(output).isEqualTo(expected);
    }

    @Test
    public void updateKegiatanSukarelaTest() throws Exception {
        KegiatanSukarela kegiatanSukarela = new KegiatanSukarela((long) 1, "pacil", "ceo",
                2016, 2018, "abc");
        String pencapianJson = new ObjectMapper().writeValueAsString(kegiatanSukarela);
        Mockito.when(
                mockSevvyService.updateKegiatanSukarela(kegiatanSukarela))
                .thenReturn(kegiatanSukarela);
        this.mvc.perform(put("/database/profile/kegiatan-sukarela/")
                .contentType(MediaType.APPLICATION_JSON).content(pencapianJson))
                .andExpect(status().isOk());

    }

    @Test
    public void deleteKegiatanSukarelaTest() throws Exception {
        String input = "{\"id\":\"1\","
                + "\"posisi\":\"ceo\","
                + "\"perusahaan\":\"pacil\","
                + "\"tahunMulai\":\"2016\","
                + "\"tahunSelesai\":\"2018\","
                + "\"deskripsi\":\"abc\"}";

        String uriInput = "/database/profile/kegiatan-sukarela/barbie.girl@gmail.com";
        String uri = "/database/profile/kegiatan-sukarela/?id=1";
        RequestBuilder requestBuilderAdd = MockMvcRequestBuilders
                .post(uriInput)
                .accept(MediaType.APPLICATION_JSON)
                .content(input)
                .contentType(MediaType.APPLICATION_JSON);

        RequestBuilder requestBuilderDelete = MockMvcRequestBuilders
                .delete(uri)
                .accept(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilderAdd).andExpect(status().isOk());
        mvc.perform(requestBuilderDelete).andExpect(status().isOk());

    }

    //Keterampilan
    @Test
    public void createKeterampilan() throws Exception {
        String inputJson = new ObjectMapper().writeValueAsString(new Keterampilan((long) 1,
                "Programming", 8));
        this.mvc.perform(post("/database/profile/keterampilan/barbie.girl@gmail.com")
                .contentType(MediaType.APPLICATION_JSON).content(inputJson))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllKeterampilanTest() throws Exception {
        Keterampilan keterampilan1 = new Keterampilan((long) 1, "makan", 100);
        Keterampilan keterampilan2 = new Keterampilan((long) 2, "eat", 100);
        keterampilan1.setUsers(user);
        keterampilan2.setUsers(user);
        List<Keterampilan> keterampilanList = new ArrayList<>();
        keterampilanList.add(keterampilan1);
        keterampilanList.add(keterampilan2);

        String inputJson = new ObjectMapper().writeValueAsString(keterampilanList);
        String uri = "/database/profile/keterampilan/all/?email=barbie.girl@gmail.com";

        Mockito.when(
                mockSevvyService.findKeterampilanByEmail("barbie.girl@gmail.com"))
                .thenReturn(keterampilanList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        MvcResult result = mvc.perform(requestBuilder).andReturn();
        String expected = new Gson().toJson(keterampilanList);
        String output = result.getResponse().getContentAsString();

        assertThat(output).isEqualTo(expected);
    }

    @Test
    public void getKeterampilanTest() throws Exception {
        Keterampilan keterampilan = new Keterampilan((long) 1, "makan", 100);
        keterampilan.setUsers(user);

        String inputJson = new ObjectMapper().writeValueAsString(keterampilan);
        String uri = "/database/profile/keterampilan/?id=1";

        Mockito.when(
                mockSevvyService.findKeterampilanById((long) 1))
                .thenReturn(keterampilan);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        MvcResult result = mvc.perform(requestBuilder).andReturn();
        String expected = new Gson().toJson(keterampilan);
        String output = result.getResponse().getContentAsString();

        assertThat(output).isEqualTo(expected);

    }

    @Test
    public void updateKeterampilanTest() throws Exception {
        Keterampilan keterampilan = new Keterampilan((long) 1, "makan", 100);
        String pencapianJson = new ObjectMapper().writeValueAsString(keterampilan);
        Mockito.when(
                mockSevvyService.updateKeterampilan(keterampilan))
                .thenReturn(keterampilan);
        this.mvc.perform(put("/database/profile/keterampilan/")
                .contentType(MediaType.APPLICATION_JSON).content(pencapianJson))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteKeterampilanTest() throws Exception {
        String input = "{\"id\":\"1\","
                + "\"namaKeterampilan\":\"makan\","
                + "\"tingkatKeterampilan\":\"100\"}";

        String uriInput = "/database/profile/keterampilan/barbie.girl@gmail.com";
        String uri = "/database/profile/keterampilan/?id=1";
        RequestBuilder requestBuilderAdd = MockMvcRequestBuilders
                .post(uriInput)
                .accept(MediaType.APPLICATION_JSON)
                .content(input)
                .contentType(MediaType.APPLICATION_JSON);

        RequestBuilder requestBuilderDelete = MockMvcRequestBuilders
                .delete(uri)
                .accept(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilderAdd).andExpect(status().isOk());
        mvc.perform(requestBuilderDelete).andExpect(status().isOk());

    }
    //CV
    @Test
    public void addCvTest() throws Exception {
        String inputJson = new ObjectMapper().writeValueAsString(new CV((long) 1, "nasu",
                "plain", 1, "eng"));
        this.mvc.perform(post("/database/cv/barbie.girl@gmail.com")
                .contentType(MediaType.APPLICATION_JSON).content(inputJson))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllCvTest() throws Exception {
        CV cv1 =  new CV((long) 1, "nasu", "plain",1, "eng");
        CV cv2 = new CV((long) 1, "nasu", "plain",1, "eng");
        cv1.setUsers(user);
        cv2.setUsers(user);
        List<CV> cvList = new ArrayList<>();
        cvList.add(cv1);
        cvList.add(cv2);

        String inputJson = new ObjectMapper().writeValueAsString(cvList);
        String uri = "/database/cv/all/?email=barbie.girl@gmail.com";

        Mockito.when(
                mockSevvyService.findCVByEmail("barbie.girl@gmail.com"))
                .thenReturn(cvList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        MvcResult result = mvc.perform(requestBuilder).andReturn();
        String expected = new Gson().toJson(cvList);
        String output = result.getResponse().getContentAsString();

        String expectedJson = "[{\"id\":1,"
                + "\"cvName\":\"nasu\","
                + "\"template\":\"plain\","
                + "\"cvLang\":\"eng\"},"
                + "{\"id\":1,"
                + "\"cvName\":\"nasu\","
                + "\"template\":\"plain\","
                + "\"cvLang\":\"eng\"}]";

        assertThat(output).isEqualTo(expectedJson);

    }

    @Test
    public void getCvTest() throws Exception {
        CV cv =  new CV((long) 1, "nasu", "plain",1, "eng");
        cv.setUsers(user);

        String inputJson = new ObjectMapper().writeValueAsString(cv);
        String uri = "/database/cv/?id=1";

        Mockito.when(
                mockSevvyService.findCVById((long) 1))
                .thenReturn(cv);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        String expectedJson = "{\"id\":1,"
                + "\"cvName\":\"nasu\","
                + "\"template\":\"plain\","
                + "\"cvLang\":\"eng\"}";

        MvcResult result = mvc.perform(requestBuilder).andReturn();
        String expected = new Gson().toJson(cv);
        String output = result.getResponse().getContentAsString();

        assertThat(output).isEqualTo(expectedJson);

    }

    @Test
    public void updateCvTest() throws Exception {
        CV cv =  new CV((long) 1, "nasu", "plain", 1, "eng");
        String pencapianJson = new ObjectMapper().writeValueAsString(cv);
        Mockito.when(
                mockSevvyService.updateCV(cv))
                .thenReturn(cv);
        this.mvc.perform(put("/database/cv/")
                .contentType(MediaType.APPLICATION_JSON).content(pencapianJson))
                .andExpect(status().isOk());

    }

    @Test
    public void deleteCvTest() throws Exception {
        String input = "{\"id\":\"1\","
                + "\"cvName\":\"nasu\","
                + "\"template\":\"plain\","
                + "\"colorCode\":1,"
                + "\"cvLang\":\"eng\"}";

        String uriInput = "/database/cv/barbie.girl@gmail.com";
        String uri = "/database/cv/?id=1";
        RequestBuilder requestBuilderAdd = MockMvcRequestBuilders
                .post(uriInput)
                .accept(MediaType.APPLICATION_JSON)
                .content(input)
                .contentType(MediaType.APPLICATION_JSON);

        RequestBuilder requestBuilderDelete = MockMvcRequestBuilders
                .delete(uri)
                .accept(MediaType.APPLICATION_JSON);

        mvc.perform(requestBuilderAdd).andExpect(status().isOk());
        mvc.perform(requestBuilderDelete).andExpect(status().isOk());
    }

    @Test
    public void getPdfModernTest() throws Exception {
        byte[] mockByte = "pdf".getBytes();
        String inputJson = new ObjectMapper().writeValueAsString(mockByte);
        Mockito.when(mockSevvyService.findUserByEmail("barbie.girl@gmail.com"))
                .thenReturn(user);
        Mockito.when(mockSevvyService.downloadPDF(mockSevvyService.findUserByEmail("barbie.girl@gmail.com"), "MODERN", 1, "ENG"))
                .thenReturn(mockByte);
        String uri = "/database/pdf/modern?email=barbie.girl@gmail.com&colorCode=1&cvName=coba&cvLang=ENG";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);
        MvcResult result = mvc.perform(requestBuilder).andReturn();
        mvc.perform(requestBuilder).andExpect(status().isOk());

    }

    @Test
    public void getPdfFormalTest() throws Exception {
        byte[] mockByte = "pdf".getBytes();
        String inputJson = new ObjectMapper().writeValueAsString(mockByte);
        Mockito.when(mockSevvyService.findUserByEmail("barbie.girl@gmail.com"))
                .thenReturn(user);
        Mockito.when(mockSevvyService.downloadPDF(mockSevvyService.findUserByEmail("barbie.girl@gmail.com"),
                "FORMAL", 3, "ENG"))
                .thenReturn(mockByte);
        String uri = "/database/pdf/formal?email=barbie.girl@gmail.com&colorCode=1&cvName=coba&cvLang=ENG";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);
        MvcResult result = mvc.perform(requestBuilder).andReturn();
        mvc.perform(requestBuilder).andExpect(status().isOk());

    }


}